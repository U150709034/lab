package stack;

public class TestStackImpl {

	public static void main(String[] args) {
		StackArrayImpl stack = new StackArrayImpl();
		
		
		stack.push('o');
		stack.push('w');
		System.out.println(stack.pop());
		System.out.println(stack.pop());
		stack.push('h');
		stack.push('a');
		System.out.println(stack.pop());
		System.out.println(stack.pop());

	    //First in last out
	
		stack.push("A");
		stack.push("B");
		stack.push("C");
		stack.push("D");
		while (!stack.empty()){
		System.out.println(stack.pop());
		}
	
	}
}
