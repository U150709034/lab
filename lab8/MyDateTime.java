package lab;

public class MyDateTime {
	
	MyDate date;
	MyTime time;

	public MyDateTime() {
		
	}

	public MyDateTime(MyDate date, MyTime time) {
		this.date = date;
		this.time = time;
	}

	public String toString() {
		return date.toString()+ " " + time.toString();
	}
	
	public void incrementDay() {
		date.incrementDay();
		
	}

	public void incrementHour() {
		date.incrementDay(time.incrementHour());
	}

	public void incrementHour(int i) {
		date.incrementDay(time.incrementHour(1));
		
	}

	public void decrementHour(int i) {
		date.decrementHour(time.decrementHour(1));
		
	}

	public void incrementMinute(int i) {
		date.incrementMinute(time.incrementMinute(1));
		
	}

	public void decrementMinute(int i) {
		date.decrementMinute(1);
		
	}

	public void incrementYear(int i) {
		date.incrementYear();
		
	}

	public void decrementDay() {
		date.decrementDay();
		
	}

	public void decrementYear() {
		
		
	}

	public void decrementMonth() {
		
		
	}

	public void incrementDay(int i) {
		
		
	}

	public void decrementMonth(int i) {
		
		
	}

	public void decrementDay(int i) {
		
		
	}

	public void incrementMonth(int i) {
		
		
	}

	public void decrementYear(int i) {
		
		
	}

	public void incrementMonth() {
		
		
	}

	public void incrementYear() {
		
		
	}

	public boolean isBefore(MyDateTime anotherDateTime) {
		
		return false;
	}

	public boolean isAfter(MyDateTime anotherDateTime) {
		
		return false;
	}

	public String dayTimeDifference(MyDateTime anotherDateTime) {
		
		return null;
	}

}
