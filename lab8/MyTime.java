package lab;

public class MyTime {

	int hour;
	int minute;
	
	public MyTime(int h, int m) {
		hour = h;
		minute = m;
	}

	public String toString() {
		return (hour < 10 ? "0" : "") + hour+":"+ (minute < 10 ? "0" : "") + minute+":";}

	public int incrementHour() {
		hour++;
		if (hour == 24) {
			hour = 0;
			return 1;
		}
		return 0;
	}

	public int incrementHour(int i) {
		int dayInc = (hour + i) / 24;
		hour = (hour + i) % 24;
		return dayInc;
	}

}
