import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		char[][] board = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };
		int row, col;

		printBoard(board);

		boolean isPrime = true, turnOne = true;

		while(isPrime){
			try{
				if(turnOne){
					System.out.print("Player 1 enter row number:");
					row = reader.nextInt();
					System.out.print("Player 1 enter column number:");
					col = reader.nextInt();

					if(board[row-1][col-1] != ' '){
						while(board[row-1][col-1] != ' '){
							System.out.println("That location is already filled. Try Again");
							System.out.print("Player 1 enter row number:");
							row = reader.nextInt();
							System.out.print("Player 1 enter column number:");
							col = reader.nextInt();
						}
						board[row - 1][col - 1] = 'X';
						printBoard(board);
						turnOne = false;
					}else{
						board[row - 1][col - 1] = 'X';
						printBoard(board);
						turnOne = false;
					}
				}

			}catch(ArrayIndexOutOfBoundsException e){
				System.out.println("The number you have entered is invalid. Please enter a number between 1 and 3");
				continue;
			}

			try {
				if(!turnOne){
					System.out.print("Player 2 enter row number:");
					row = reader.nextInt();
					System.out.print("Player 2 enter column number:");
					col = reader.nextInt();

					if(board[row-1][col-1] != ' '){
						while(board[row-1][col-1] != ' '){
							System.out.println("That location is already filled. Try Again");
							System.out.print("Player 2 enter row number:");
							row = reader.nextInt();
							System.out.print("Player 2 enter column number:");
							col = reader.nextInt();
						}
						board[row - 1][col - 1] = 'O';
						printBoard(board);
						turnOne = true;
					}else{
						board[row - 1][col - 1] = 'O';
						printBoard(board);
						turnOne = true;
					}
				}
			} catch(ArrayIndexOutOfBoundsException e){
				System.out.println("The number you have entered is invalid. Please enter a number between 1 and 3");
				continue;
			}

		}
		reader.close();
	}

	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");

			}
			System.out.println();
			System.out.println("   -----------");

		}

	}

}
