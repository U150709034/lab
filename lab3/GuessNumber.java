

import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {	
		Scanner reader = new Scanner(System.in); //Creates an object to read user input
		Random rand = new Random(); //Creates an object from Random class
		int number =rand.nextInt(100); //generates a number between 0 and 99
		
		
		System.out.println("Hi! I'm thinking of a number between 0 and 99.");
		System.out.print("Can you guess it: ");
		
		int guess = reader.nextInt(); //Read the user input
		int attempts = 1;
		if(guess == number){
			System.out.println("Congratulations");
		}else{			
			while (guess != -1 ){
				if(guess < number){
					System.out.println("Sorry");
					System.out.println("Mine is greater than your guess.");
					System.out.println("Type -1 to exit or try another: ");
				}else if(guess >number){
					System.out.println("Sorry");
					System.out.println("Mine is lesser than your guess.");
					System.out.println("Type -1 to exit or try another: ");
				}else{
					System.out.println("Congratulations! You won after "+attempts+" attempts!");
					break;
				}
				guess = reader.nextInt();
				attempts += 1;
			}
			
			reader.close();
		}
		
		
		
	}

}