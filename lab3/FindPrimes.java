public class FindPrimes{
	public static void main(String[] args){
		int value = Integer.parseInt(args[0]);
		
		for(int i = 2;i <= value; i++){
			int divisor = 2;
			boolean isPrime = true;
			while ((divisor < i)&&(isPrime)){
				if(i % divisor == 0){
					isPrime = false;
					break;
				}
				divisor++;
			}
			if(isPrime){
				System.out.println(i);
			}
		}
	}
}