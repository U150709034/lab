package generics;

import java.util.ArrayList;
import java.util.List;

public class StackArrayImpl<T> implements Stack<T> {
	
	ArrayList items = new ArrayList();
	@Override
	public void push(T obj) {
		items.add(obj);
	}

	@Override
	public T pop() {
		return (T) items.remove(items.size()-1);
	}

	@Override
	public boolean empty() {
		return items.size() == 0;
	}

	@Override
	public List<T> toList() {
		return items;
	}

	@Override
	public void addAll(Stack<T> aStack) {
		List<T> list = aStack.toList();
		
		
	}

}
