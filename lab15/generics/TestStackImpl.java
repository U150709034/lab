package generics;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class TestStackImpl<T> {

	public static void main(String[] args) {
		List list = new LinkedList();
		Stack <String> stack = new StackArrayImpl<>();
			
		Stack <String> stack2 = new StackArrayImpl<>();
		
		
		List<Integer> lst = new LinkedList<>();
		lst.add(5);
		lst.add(6);
		
		List<String> lstStr = new LinkedList<>();
		lstStr.add("A");
		lstStr.add("B");
		
		List<Object> lstObj = new ArrayList<>();
		
		lstObj.addAll(lstStr);
		lstObj.addAll(lst);
		
	
	    //First in last out
	
		stack.push("A");
		stack.push("B");
		stack2.push("C");
		stack2.push("D");
		
		
		while (!stack.empty()){
		System.out.println(stack.pop());
		
		Stack<Integer> stackInt = new StackArrayImpl<>();
		stackInt.push(5);
		int a = stackInt.pop();
		
		Integer b;
		int c;
		c = 8;
		b = c;
		
		
		}
	
	}
}
