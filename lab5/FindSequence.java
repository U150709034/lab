public class FindSequence {

    static boolean founded, reversed = false;
    static int[][] tempMat = new int[10][10];

    private static void PrintMat(int[][] arr, boolean copyArr){
        for (int row = 0; row < 10; row ++ ) {
            for (int column = 0; column < 10; column++) {
                System.out.print(arr[row][column] + " ");
            }
            if(copyArr)
                tempMat[row] = arr[row].clone();
            System.out.print("\n");
        }
    }

    public static void main(String[] args){

        int[][] matrix = {
                {2, 8, 3, 7, 4, 7, 2, 4, 7, 5},
                {7, 7 ,8, 5 ,8 ,5, 9, 4, 1, 7},
                {5 ,8 ,3 ,2 ,1 ,3 ,2 ,9 ,8 ,1},
                {1 ,0 ,4 ,7 ,0 ,5 ,0 ,9 ,0 ,5},
                {8 ,6 ,5 ,0 ,6 ,0 ,1 ,2 ,1 ,9},
                {9 ,7 ,8 ,5 ,9 ,0 ,4 ,3 ,2 ,6},
                {9 ,4 ,9 ,5 ,9 ,0 ,4 ,8 ,7 ,7},
                {9 ,3 ,6 ,3 ,9 ,3 ,3 ,7 ,8 ,4},
                {6 ,6 ,9 ,7 ,0 ,3 ,5 ,6 ,7 ,1},
                {7 ,1 ,1 ,7 ,6 ,4 ,5 ,3 ,1 ,1} };


        System.out.print("Old matrix: \n");
        PrintMat(matrix, true);

        for (int row = 1; row < 10; row ++ ) {
            for (int column = 1; column < 10; column++) {

                if(matrix[row][column] == 0)
                    founded = SearchMat(matrix, row, column);
                if(founded){ 
                    if(reversed) 
                        break;
                    else
                        
                        SearchMat(matrix, row, column);

                    
                    tempMat[row][column] = 9;
                }
            }
        }

        System.out.println("New matrix: ");
        PrintMat(tempMat,false);
    }

    private static boolean SearchMat(int[][] arr, int row, int col){

        int currentValue = arr[row][col], nextValue = (currentValue +1), tempRow, tempCol;

        tempRow = row-1; tempCol = col-1;

        if(!founded) {

            if (arr[row][col] == 9) {
                founded = true;
                return true;
            }

            if (tempRow <= -1 || tempCol <= -1)
                return false;

            if (arr[row - 1][col] == nextValue)
                return SearchMat(arr, row - 1, col);
            else if (arr[row + 1][col] == nextValue)
                return SearchMat(arr, row + 1, col);
            else if (arr[row][col - 1] == nextValue)
                return SearchMat(arr, row, col - 1);
            else if (arr[row][col + 1] == nextValue)
                return SearchMat(arr, row, col + 1);
            else {
                return false;
            }
        }else{
            

            int calcValue = 9 - nextValue;

            if (calcValue == -1){
                reversed = true;
                return true;
            }

            if (tempRow <= -1 || tempCol <= -1)
                return false;

            if (arr[row - 1][col] == nextValue){
                tempMat[row -1][col] = calcValue;
                return SearchMat(arr,row - 1, col);
            } else if (arr[row + 1][col] == nextValue){
                tempMat[row+1][col] = calcValue;
                return SearchMat(arr, row + 1, col);
            } else if (arr[row][col - 1] == nextValue){
                tempMat[row][col-1] = calcValue;
                return SearchMat(arr, row, col - 1);
            } else if (arr[row][col + 1] == nextValue){
                tempMat[row][col + 1] = calcValue;
                return SearchMat(arr, row, col + 1);
            } else {
                return false;
            }

        }
    }

}