public class GCDRec{
	public static void main (String [] args){
		
		int x = Integer.parseInt(args[0]);
		int y = Integer.parseInt(args[1]);
		System.out.println(gcdrec(x,y));
	}
	
	public static int gcdrec(int x, int y){
		if ( y == 0)
			return x;
		else if ( x>= y && y >0)
			return gcdrec (y, x%y);
		else return gcdrec (y,x);
	}
}