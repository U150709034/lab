public class GCDLoop{
	public static void main (String[] args){
		int a = Integer.parseInt(args[0]);
		int b = Integer.parseInt(args[1]);
		int r = 0;
		
		for (int i = 1; i <= a && i <= b; ++i){
			if(a % i == 0 && b % i == 0)
				r = i;
		}
		
		System.out.println("GCD of "+a+" and "+b+" is "+r);
	
	}
}