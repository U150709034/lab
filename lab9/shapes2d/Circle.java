package shapes2d;

public class Circle {
	protected int radius; //public/protected olsun ki silindir paketinden kullanabilelim
	
	public Circle(int radius) {
		this.radius=radius;
	}
	
	public double area() {
		return Math.PI*(radius*radius);
	}
	
	public String toString() {
		return "radius = " + radius;
	}
}
