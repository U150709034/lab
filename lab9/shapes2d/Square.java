package shapes2d;

import java.lang.Object;

public class Square {
	protected int side;
	
	public Square(int side) {
		this.side = side;
		
	}
	
	public double area() {
		return side*side;
	}
	
	public String toString() {
		return "side = " + side;
	}
}
