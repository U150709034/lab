package shapes2d;

public class Test2dShapes {

	public static void main(String[] args) {
		
		Circle circle = new Circle(5);
		System.out.println(circle.area());
		
		String str2 = circle.toString();
		System.out.println(str2);
		
		Square squ = new Square(6);
		System.out.println(squ.area());
		
		String str = squ.toString();
		System.out.println(str);
		
				
	
	}

}
