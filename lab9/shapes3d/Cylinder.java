package shapes3d;

import shapes2d.Circle;

public class Cylinder extends Circle{
	
	int h;
	public Cylinder(int radius,int h) {
		super(radius);//this super() must be in first line always
		this.h = h;
		
	}
	
	public double area() {
		return 2*super.area()+(2*Math.PI*radius*h);
	}
	
	public double volume() {
		return super.area()*h; //super.area() means overriding method
				
	}
	
	@Override
	public String toString() {
		return super.toString()+" height= "+h;
	}
		
}
