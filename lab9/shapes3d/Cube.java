package shapes3d;

import shapes2d.Square;

public class Cube extends Square{
	
	public Cube(int side) {
		super(side);
		
	}
	
	public double area() {
	
		return 6*super.area();
	}
	
	public double volume() {
		return super.area()*side;
	}
}
