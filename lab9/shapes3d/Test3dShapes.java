package shapes3d;

public class Test3dShapes {

	public static void main(String[] args) {
		Cylinder cyl = new Cylinder(5,10);
		double area = cyl.area();
		double vol = cyl.volume();
		
		System.out.println(area);
		System.out.println(vol);
		System.out.println(cyl);
		
		Cube cube = new Cube(6);
		double area1 = cube.area();
		double vol1 = cube.volume();
		
		System.out.println(area1);
		System.out.println(vol1);
		System.out.println(cube);
		
	}

}
