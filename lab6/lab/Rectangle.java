package lab;

public class Rectangle {
	
	int sideA, sideB;
	Point topLeft = new Point(sideA, sideB);
	
	public Rectangle(int x,int y) {
		sideA = x;
		sideB = y;
	}
	
	public static int area(int x,int y) {
		return x*y;
	}
	
	public int perimeter(int x, int y) {
		return 2*(x+y);
	}
	
	public Point[] corners(){
        Point[] points = new Point[4];
        points[0] = new Point(topLeft.x, topLeft.y - sideA);
        points[1] = topLeft;
        points[2] = new Point(topLeft.x + sideB, topLeft.y - sideA);        
        points[3] = new Point(topLeft.x + sideB, topLeft.y);

        return points;
    }

}
		

