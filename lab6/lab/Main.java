package lab;

public class Main {

	public static void main(String[] args) {
	Point topLeft = new Point(0,15);
	Rectangle rectangle1 = new Rectangle(5,12);
	System.out.println("x: " + rectangle1.sideA + ", y: " + rectangle1.sideB);
	
	System.out.println(rectangle1.area(5, 12));
	System.out.println(rectangle1.perimeter(5, 12));
	System.out.print("Corners: \n");
    for(int i = 0; i < 4; i++){
         System.out.println(rectangle1.corners()[i].x + " " + rectangle1.corners()[i].y);

    }	}

}
